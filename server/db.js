const mongoose = require('mongoose');
const fs = require('fs');
const join = require('path').join;
const models = join(__dirname, '/models');
const countries = require('./models/data/countries.json');

mongoose.connect(process.env.DB_URL, { useNewUrlParser: true });
mongoose.Promise = global.Promise;

// Bootstrap models
fs.readdirSync(models)
  .filter(file => ~file.search(/^[^\.].*\.js$/))
  .forEach(file => require(join(models, file)));

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
db.once('open', () => {
  console.log('DB is connected');
});

const Country = mongoose.model('Country');

Country.countDocuments()
  .then((count) => {
    if (count > 0) {
      return;
    }

    mongoose.modelNames().forEach((name) => {
      const model = mongoose.model(name);
      if (model.fill && typeof model.fill === 'function') {
        model.fill();
      }
    });
  });

// mongoose.model('Country').collection.insertMany(countries, function (err, r) {
//   console.log(r);
// });


module.exports = db;
