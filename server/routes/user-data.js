const router = require('express').Router();
const mongoose = require('mongoose');

router.get('/countries', async (req, res, next) => {
  try {
    const result = await mongoose.model('Country').find({}, { _id: 0 });
    res.json(result);
  } catch(error) {
    next(error);
  }
});


router.get('/cities/:id', async (req, res, next) => {
  try {
    const result = await mongoose.model('City').find({ countryId: Number(req.params.id) }, { _id: 0, countryId: 0 });
    res.json(result.map(item => item.name));
  } catch(error) {
    next(error);
  }
});

router.get('/zip-codes/:cityName', async (req, res, next) => {
  try {
    const result = await mongoose.model('ZipCode').findOne({ city: req.params.cityName }, { _id: 0, codes: 1 });
    res.json(result.codes);
  } catch(error) {
    next(error);
  }
});

module.exports = router;