const router = require('express').Router();
const { documentToObject } = require('../utils');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const jwtExpress = require('express-jwt');
const User = mongoose.model('User');

router.get('/', jwtExpress({ secret: process.env.JWT_SECRET }), async(req, res, next) => {
  try {
    const userDocument = await User.findOne({ _id: req.user.subject });
    const user = documentToObject(userDocument);
    if (!user) {
      res.status(404).send('User not exist.');
    }

    if (user.country !== undefined) {
      const countryModel = await mongoose.model('Country').findOne({ id: user.country });
      user.country = countryModel.name;
    }
    delete user.password;
    res.json(user);
  } catch (error) {
    next(error);
  }
});

router.post('/register', async(req, res, next) => {
  try {
    const createdUser = await User.create(req.body);
    const token = jwt.sign({ subject: createdUser._id }, process.env.JWT_SECRET);
    res.json(token);
  } catch (error) {
    next(error);
  }
});

router.post('/login', async(req, res, next) => {
  try {
    const { user, password } = req.body;
    const result = await User.findOne({ user, password });
    if (!result) {
      res.status(404).send('User not exist.');
    }
    const token = jwt.sign({ subject: result._id }, process.env.JWT_SECRET);
    res.json(token);
  } catch (error) {
    next(error);
  }
});

module.exports = router;