const mongoose = require('mongoose');
const countries = require('./data/countries.json');
const Schema = mongoose.Schema;

const countrySchema = new Schema({
  name: { type: String, required: true, unique: true },
  id: { type: Number, required: true, unique: true }
});

countrySchema.statics = {
  fill() {
    this.collection.insertMany(countries).catch(console.error);
  }
};

mongoose.model('Country', countrySchema);
