const mongoose = require('mongoose');
const cities = require('./data/cities.json');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
  email: { type: String, required: true },
  password: { type: String, required: true },
  country: { type: Number },
  city: { type: String },
  zipCode: { type: String },
});

mongoose.model('User', UserSchema);
