const mongoose = require('mongoose');
const cities = require('./data/cities.json');
const Schema = mongoose.Schema;

const citySchema = new Schema({
  name: { type: String, required: true} ,
  countryId: { type: Number, required: true }
});

citySchema.statics = {
  fill() {
    this.collection.insertMany(cities).catch(console.error);
  }
};

mongoose.model('City', citySchema);
