const mongoose = require('mongoose');
const zipCodes = require('./data/zip-codes.json');
const Schema = mongoose.Schema;

const zipSchema = new Schema({
  city: { type: String, required: true },
  codes: [{ type: Number }]
});

zipSchema.statics = {
  fill() {
    this.collection.insertMany(zipCodes).catch(console.error);
  }
};

mongoose.model('ZipCode', zipSchema);
