module.exports = {
  documentToObject (document) {
    let object = JSON.parse(JSON.stringify(document));
    delete object._id;
    delete object.__v;
    return object;
  }
};