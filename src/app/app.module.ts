import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { NavigationComponent } from './components/navigation/navigation.component';
import { HomeComponent } from './components/home/home.component';
import { AuthService } from './services/auth.service';
import { UserDataService } from './services/user-data.service';
import { UserService } from './services/user.service';
import { AuthGuard } from './guards/auth.guard';
import { RestoreComponent } from './components/restore/restore.component';
import { SignUpComponent } from './components/sing-up/sign-up.component';
import { LoginComponent } from './components/login/login.component';
import { CustomSelectComponent } from './components/custom-select/custom-select.component';
import { HttpClientModule } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    NavigationComponent,
    RestoreComponent,
    CustomSelectComponent,
    SignUpComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    RouterModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  bootstrap: [
    AppComponent,
  ],
  providers: [
    AuthGuard,
    AuthService,
    UserService,
    UserDataService,
  ]
})
export class AppModule {
}
