import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class AuthService {
  private token: string;

  constructor(private router: Router,) {
  }

  public get isLoggedIn(): boolean {
    return Boolean(this.token);
  }

  public assignToken(token: string): void {
    this.token = token;
  }

  public logout(): void {
    this.token = null;
    this.router.navigate(['/login']);
  }

  public getToken(): string {
    return this.token;
  }
}
