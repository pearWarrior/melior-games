import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class UserDataService {

  private readonly path: string;

  constructor(private http: HttpClient,) {
    this.path = `${ environment.apiUrl }user-data`;
  }

  public fetchCountries(): Promise<Country[]> {
    return this.http.get(`${ this.path }/countries`).toPromise();
  }

  public fetchCities(countryId: string): Promise<string[]> {
    return this.http.get(`${ this.path }/cities/${countryId}`).toPromise();
  }

  public fetchZipCodes(cityName: string): Promise<number[]> {
    return this.http.get(`${ this.path }/zip-codes/${cityName}`).toPromise();
  }
}

export interface Country {
  id: number;
  name: string;
}
