import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';
import { AuthService } from "./auth.service";

export interface IUser {
  email: string;
  password: string;
  country: number;
  city?: string;
  zipCode?: number;
}

export interface IUserData {
  email: string;
  country: string;
  city?: string;
  zipCode?: number;
}

@Injectable()
export class UserService {

  private readonly path: string;

  constructor(private http: HttpClient, private authService: AuthService,) {
    this.path = `${ environment.apiUrl }user`;
  }

  public get(): Promise<IUserData> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.authService.getToken()}`,
      })
    };
    return this.http.get(`${ this.path }`, httpOptions).toPromise();
  }

  public register(user: IUser): Promise<string> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    Object.keys(user).forEach((key) => (user[key] == null) && delete user[key]);
    return this.http.post(`${ this.path }/register`, user, httpOptions).toPromise()
      .then((token: string) => {
        this.authService.assignToken(token);
        return token;
      });
  }

  public login(username: string, password: string): Promise<string> {
    return this.http.post(`${ this.path }/login`, { username, password }).toPromise()
      .then((token: string) => {
        this.authService.assignToken(token);
        return token;
      });
  }
}
