import { Component, Input, Output, EventEmitter } from '@angular/core';

export interface OptionItem {
  value: string | number;
  name: string;
}

export const UN_SELECT = null;

@Component({
  selector: 'custom-select',
  templateUrl: 'custom-select.component.html'
})
export class CustomSelectComponent {
  private _options: OptionItem[] = [];

  @Input()
  set options(data: OptionItem[]) {
    if (data && data.length) {
      data.unshift({ value: UN_SELECT, name: this.chooseMessage });
    }
    this._options = data;
  }
  get options(): OptionItem[] {
    return this._options;
  }

  @Input()
  chooseMessage: string;

  @Output()
  valueChange: EventEmitter<string | number>;

  constructor() {
    this.valueChange = new EventEmitter();
  }

  onChange(value: string) {
    this.valueChange.emit(value === 'null' ? null : value);
  }
}