import { Component } from '@angular/core';
import { UserService } from "../../services/user.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: [ './login.component.scss' ],
})
export class LoginComponent {
  private login: string;
  private password: string;
  private error: string;

  constructor(private userService: UserService, private router: Router) {

  }

  private onSubmit(): void {
    this.error = null;
    this.userService.login(this.login, this.password)
      .then(() => {
        this.router.navigate(['/home']);
      })
      .catch(error=> this.error = error.error);
  }
}
