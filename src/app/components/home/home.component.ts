import { Component } from '@angular/core';
import { UserService, IUserData } from "../../services/user.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent {
  private user: IUserData;
  private loading: boolean;

  constructor(userService: UserService, router: Router) {
    this.loading = true;
    userService.get()
      .then((user: IUserData) => {
        this.user = user;
        this.loading = false;
      })
      .catch((error) => {
        console.error(error);
        router.navigate(['/login']);
      });
  }
}
