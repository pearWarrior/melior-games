import { Component } from '@angular/core';
import { UserDataService, Country } from "../../services/user-data.service";
import { OptionItem, UN_SELECT } from "../custom-select/custom-select.component";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService, IUser } from "../../services/user.service";
import { AuthService } from "../../services/auth.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
})
export class SignUpComponent {
  private countries: OptionItem[] = [];
  private cities: OptionItem[];
  private zipCodes: OptionItem[];
  private form: FormGroup;

  private user: IUser = { email: '', password: '', country: null, city: null, zipCode: null };

  constructor(private userDataService: UserDataService,
              private userService: UserService,
              private authService: AuthService,
              private router: Router,
              private formBuilder: FormBuilder) {

    this.form = this.formBuilder.group({
      email: [null, Validators.required],
      password: [null, Validators.required],
      passwordConfirm: [null, Validators.required],
    });

    userDataService.fetchCountries().then((result: Country[]) => {
      this.countries = result.map((country: Country): OptionItem => {
        return { ...country, value: String(country.id) };
      });
    });
  }

  onCountryChange(value: string): void {
    this.cities = [];

    if (value === UN_SELECT) {
      this.cities = null;
      this.onCityChange(UN_SELECT);
      return;
    }
    this.user.country = Number(value);
    this.userDataService.fetchCities(value)
      .then((result: string[]) => {
        this.cities = result.map(city => ({ value: city, name: city }));
      });
  }

  onCityChange(cityName: string): void {
    this.zipCodes = [];
    if (cityName === UN_SELECT) {
      this.zipCodes = null;
      return;
    }
    this.user.city = cityName;
    this.userDataService.fetchZipCodes(cityName)
      .then((result: number[]) => {
        this.zipCodes = result.map(code => ({ value: code, name: String(code) }));
      });
  }

  onZipCodeChange(zipCode: number): void {
    this.user.zipCode = zipCode;
  }

  onSubmit(): void {
    let { password, passwordConfirm } = this.form.controls;
    if (password.value !== passwordConfirm.value) {
      return this.form.get('passwordConfirm').setErrors({ noEqual: true });
    }
    this.userService.register(this.user)
      .then((token: string) => {
        this.authService.assignToken(token);
        this.router.navigate(['/home']);
      })
      .catch(console.error);
  }
}
